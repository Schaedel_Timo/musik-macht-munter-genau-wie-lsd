package view;
 
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import logic.Song;
import logic.SongAndVoteWrapper;

public class Playlist extends SongListPanel { 

	private JPanel contentPane;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Playlist frame = new Playlist();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Playlist() {														//MainPanelerstellung
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setBackgroundImageBorderLayout(contentPane);
		contentPane = (JPanel)getContentPane();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		JPanel navPanel = new JPanel();										//NavigationsPanel
		navPanel.setLayout(new FlowLayout());
		navPanel.add(btn_logout);
		navPanel.add(btn_songAdd);
		navPanel.add(btn_liste);
		navPanel.add(btn_changePW);
		navPanel.add(btn_addUser);
		navPanel.setOpaque(false);
		contentPane.add(navPanel, BorderLayout.NORTH);
		
		JPanel grid = new JPanel();											//PlaylistVoting
		grid.setLayout(new GridBagLayout());
		grid.setBackground(Color.decode("#EFCE74"));
		GridBagConstraints c = new GridBagConstraints();
		c.weightx = 0.5;
		c.ipadx = 20;
		c.ipady = 10;
		c.gridx = 0;
		c.gridy = 0;
		grid.add(new JLabel("Bandname"), c);
		
		c.gridx = 1;
		c.gridy = 0;
		grid.add(new JLabel("Titelname"), c);
		
		c.gridx = 2;
		c.gridy = 0;
		grid.add(new JLabel("Genre"), c);
		
		c.gridx = 3;
		c.gridy = 0;
		grid.add(new JLabel("Votes"), c);
		
		int y = 1;										//Listeninitialisierung
		List<SongAndVoteWrapper> songs =  logic.Logic.getSortedListWithVotes();
		for (SongAndVoteWrapper s: songs) {
			 
			c.gridx = 0;
			c.gridy = y;
			grid.add(new JLabel(s.getSong().getBandName()), c);
			
			c.gridx = 1;
			c.gridy = y;
			grid.add(new JLabel(s.getSong().getTitelName()), c);
			
			c.gridx = 2;
			c.gridy = y;
			grid.add(new JLabel(s.getSong().getGenre()), c);
			
			c.gridx = 3;
			c.gridy = y;
			grid.add(new JLabel("" + s.getVotes()), c);
			
			y++;
		}
			
		JScrollPane scroll = new JScrollPane(grid);						//Scrollbar
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		contentPane.add(scroll, BorderLayout.CENTER);

	}
}
