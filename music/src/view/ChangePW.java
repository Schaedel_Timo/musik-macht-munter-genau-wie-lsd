package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import logic.Logic;


public class ChangePW extends Playlist { 
	private JPanel contentPane;
	JLabel l_oldpw = new JLabel("altes passwort");
	JLabel l_newpw = new JLabel("neues passwort");
	JTextField txtoldPW = new JTextField("");
	JTextField txtnewPW = new JTextField("");
	JButton btn_submit2 = new JButton("submit");
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChangePW frame = new ChangePW();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public ChangePW() {												//MainPanelerstellung
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setBackgroundImageBorderLayout(contentPane);
		contentPane = (JPanel)getContentPane();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		JPanel navPanel = new JPanel();								//NavigationsPanel erstellung
		navPanel.setLayout(new FlowLayout());
		navPanel.add(btn_logout);
		navPanel.add(btn_liste);
		navPanel.add(btn_songAdd);
		navPanel.add(btn_checkadmin);
		navPanel.add(btn_addUser);
		navPanel.setOpaque(false);
		contentPane.add(navPanel, BorderLayout.NORTH);
		
		
		JPanel grid = new JPanel();									//PWchange Panel
		grid.setLayout(new GridBagLayout());
		grid.setOpaque(false);
		contentPane.add(grid, BorderLayout.CENTER);
		GridBagConstraints c = new GridBagConstraints();
		
		l_oldpw.setForeground(Color.white);
		c.gridx = 0;
		c.gridy = 1;
		grid.add(l_oldpw, c);
	
		l_newpw.setForeground(Color.white);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipadx = 100;
		c.gridy = 2;
		grid.add(txtoldPW, c);

		
		c.fill = GridBagConstraints.CENTER;
		c.ipadx = 0;
		c.gridy = 3;
		grid.add(l_newpw, c);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipadx = 100;
		c.gridy = 4;
		
		grid.add(txtnewPW, c);
		
		c.gridy = 5;
		grid.add(btn_submit2, c);
		
		btn_submit2.addActionListener(new ActionListener() {				//changePW ActionListener
			public void actionPerformed (ActionEvent e){
				if (logic.Logic.updatePassword(getTxtlogin(), txtoldPW.getText(), txtnewPW.getText()));
			}
		});
	}
}
	

