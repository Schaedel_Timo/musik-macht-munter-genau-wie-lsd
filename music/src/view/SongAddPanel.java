package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import logic.Logic;


public class SongAddPanel extends LoginPanel { 
	private String band;
	private String titel;
	private String genre;
	private JPanel contentPane;
	JButton btn_submit = new JButton("submit");
	JButton btn_logout = new JButton("");
	JButton btn_liste = new JButton("");
	JButton btn_checkadmin = new JButton("");
	JButton btn_changePW = new JButton("");
	JButton btn_addUser = new JButton("");
	JTextField txtband = new JTextField();
	JTextField txttitel = new JTextField();
	JTextField txtgenre = new JTextField();
	JLabel l_bandname = new JLabel("Bandname");
	JLabel l_titel = new JLabel("Titel");
	JLabel l_genre = new JLabel("Genre");

	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SongAddPanel frame = new SongAddPanel();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public SongAddPanel() {												//Mainpanelerstellung
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setBackgroundImageBorderLayout(contentPane);
		contentPane = (JPanel)getContentPane();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		JPanel navPanel = new JPanel();
		navPanel.setLayout(new FlowLayout());
		navPanel.add(btn_logout);
		navPanel.add(btn_liste);
		navPanel.add(btn_changePW);
		navPanel.add(btn_checkadmin);
		navPanel.add(btn_addUser);
		navPanel.setOpaque(false);
		contentPane.add(navPanel, BorderLayout.NORTH);
		
		
		JPanel grid = new JPanel();										//Song hinzufügen Panel
		grid.setLayout(new GridBagLayout());
		grid.setOpaque(false);
		contentPane.add(grid, BorderLayout.CENTER);
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridx = 0;
		c.gridy = 1;
		grid.add(l_bandname, c);
		l_bandname.setForeground(Color.WHITE);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipadx = 100;
		c.gridy = 2;
		grid.add(txtband, c);
		l_titel.setForeground(Color.WHITE);
		
		c.fill = GridBagConstraints.CENTER;
		c.ipadx = 0;
		c.gridy = 3;
		grid.add(l_titel, c);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipadx = 100;
		c.gridy = 4;
		
		grid.add(txttitel, c);
		
		c.fill = GridBagConstraints.CENTER;
		c.ipadx = 0;
		c.gridy = 5;
		grid.add(l_genre, c);
		l_genre.setForeground(Color.WHITE);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipadx = 100;
		c.gridy = 6;
		grid.add(txtgenre, c);
		
		c.gridy = 7;
		grid.add(btn_submit, c);
		
		 try {															
			    Image img = ImageIO.read(getClass().getResource("switch-turn-off-icon.png"));
			    Image imgLogout = img.getScaledInstance( 20, 20,  java.awt.Image.SCALE_SMOOTH ) ; 
			    btn_logout.setIcon(new ImageIcon(imgLogout));
			    
			  } catch (Exception ex) {
			    System.out.println(ex);
			  }
		 
		 try {							//https://cdn3.iconfinder.com/data/icons/ui-beast-1/32/UI-10-512.png
			    Image img = ImageIO.read(getClass().getResource("changePW.png"));
			    Image newimg = img.getScaledInstance( 20, 20,  java.awt.Image.SCALE_SMOOTH ) ; 
			    btn_changePW.setIcon(new ImageIcon(newimg));
			    
			  } catch (Exception ex) {
			    System.out.println(ex);
			  }
		 
		 try {							//https://png.icons8.com/metro/1600/list.png
			    Image img = ImageIO.read(getClass().getResource("liste.png"));
			    Image newimg = img.getScaledInstance( 20, 20,  java.awt.Image.SCALE_SMOOTH ) ; 
			    btn_liste.setIcon(new ImageIcon(newimg));
			    
			  } catch (Exception ex) {
			    System.out.println(ex);
			  }
		 
		 try {							//https://www.labsrc.com/wp-content/uploads/2018/04/uac-shield.png
			    Image img = ImageIO.read(getClass().getResource("admincheck.png"));
			    Image newimg = img.getScaledInstance( 20, 20,  java.awt.Image.SCALE_SMOOTH ) ; 
			    btn_checkadmin.setIcon(new ImageIcon(newimg));
			    
			  } catch (Exception ex) {
			    System.out.println(ex);
			  }
		 
		 try {							//https://www.labsrc.com/wp-content/uploads/2018/04/uac-shield.png
			    Image img = ImageIO.read(getClass().getResource("addUser.png"));
			    Image newimg = img.getScaledInstance( 20, 20,  java.awt.Image.SCALE_SMOOTH ) ; 
			    btn_addUser.setIcon(new ImageIcon(newimg));
			    
			  } catch (Exception ex) {
			    System.out.println(ex);
			  }
		
		btn_submit.addActionListener(new ActionListener() {					//Song hinzufügen
			public void actionPerformed (ActionEvent e){
				band = txtband.getText();
				titel = txttitel.getText();
				genre = txtgenre.getText();
				
				if(!(band.isEmpty()||titel.isEmpty()||genre.isEmpty())) {
					Logic.addSong(titel, band, genre);
					txttitel.setText("");
					txtband.setText("");
					txtgenre.setText("");
				}else {
					JOptionPane.showMessageDialog(contentPane,
						    "Please fill out every textfield",
						    "Error",
						    JOptionPane.ERROR_MESSAGE);
				}
				
				
			}
		});
		
		btn_logout.addActionListener(new ActionListener() {				//ausloggen
			public void actionPerformed (ActionEvent e){
				CardHub.goToPanel1();
			}
		});
		
		btn_liste.addActionListener(new ActionListener() {				//zur Songliste
			public void actionPerformed (ActionEvent e){
				CardHub.goToPanel3();
				
			}
		});
		
		btn_checkadmin.addActionListener(new ActionListener() {			//admincheck sortierte Liste
			public void actionPerformed (ActionEvent e){
				if (LoginPanel.getTxtlogin().equalsIgnoreCase("admin")) {
					
					CardHub.goToPanel4();
				} else {
					
					JOptionPane.showMessageDialog(contentPane,
						    "Du bist kein Admin",
						    "Fehler",
						    JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		
		btn_addUser.addActionListener(new ActionListener() {			//admincheck sortierte Liste
			public void actionPerformed (ActionEvent e){
				if (LoginPanel.getTxtlogin().equalsIgnoreCase("admin")) {
					
					CardHub.goToPanel6();
				} else {
					
					JOptionPane.showMessageDialog(contentPane,
						    "Du bist kein Admin",
						    "Fehler",
						    JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		
		btn_changePW.addActionListener(new ActionListener() {			//PW change
			public void actionPerformed (ActionEvent e){
				CardHub.goToPanel5();
			}
		});
	}
}
	

