package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class LoginPanel extends JFrame { 

	private JPanel contentPane;
	private JTextField txtlogin = new JTextField();
	private JTextField txtPasswort = new JTextField();
	private JLabel l_login = new JLabel("Login");
	private JLabel l_Passwort = new JLabel("Passwort");
	private static String textLogin;
	private static String textPasswort;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginPanel frame = new LoginPanel();
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public LoginPanel() {											//MainPanelerstellung
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setBackgroundImageGBL(contentPane);
		contentPane = (JPanel)getContentPane();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		contentPane.add(l_login, c);
		l_login.setForeground(Color.WHITE);
		c.ipadx = 200;
		c.gridx = 0;
		c.gridy = 1;
		c.fill = GridBagConstraints.HORIZONTAL;
		contentPane.add(txtlogin, c);
		l_Passwort.setForeground(Color.WHITE);
		c.ipadx = 0;
		c.gridx = 0;
		c.gridy = 2;
		c.fill = GridBagConstraints.NONE;
		contentPane.add(l_Passwort, c);
		c.ipadx = 200;
		c.gridx = 0;
		c.gridy = 3;
		c.fill = GridBagConstraints.HORIZONTAL;
		contentPane.add(txtPasswort, c);
		
		
			txtPasswort.addActionListener(new ActionListener() {							//Einlog Methoden
				public void actionPerformed(ActionEvent e) {
					textLogin = txtlogin.getText();
					textPasswort = txtPasswort.getText();
						if (logic.User.checkIfUserExists(textLogin)) {
							if (logic.Logic.checkPassword(LoginPanel.getTxtlogin(), LoginPanel.getTxtPasswort()))
								CardHub.goToPanel2();
							txtlogin.setText("");
							txtPasswort.setText("");
						}
				}
			});
			
			txtlogin.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					textLogin = txtlogin.getText();
					textPasswort = txtPasswort.getText();
						if (logic.User.checkIfUserExists(textLogin)) {
							if (logic.Logic.checkPassword(LoginPanel.getTxtlogin(), LoginPanel.getTxtPasswort()))
								CardHub.goToPanel2();
							txtlogin.setText("");
							txtPasswort.setText("");
						}
				}
			});
		}
		

	public static String getTxtlogin() {											//Benutzer
		return textLogin;
	}
	
	public static String getTxtPasswort() {											//Passwort
		return textPasswort;
	}
	
	public void setBackgroundImageGBL(JPanel contentPane) {						//https://wall.alphacoders.com/big.php?i=178560&lang=German
	
		try {
		    final Image backgroundImage = ImageIO.read(getClass().getResource("wallpaper.jpg"));
		    Image newimg = backgroundImage.getScaledInstance( 900, 350,  java.awt.Image.SCALE_SMOOTH ) ; 
		    setContentPane(new JPanel(new GridBagLayout()) {
		        @Override public void paintComponent(Graphics g) {
		            g.drawImage(newimg, 0, 0, null);
		        }
		    });
		   
		} catch (IOException e) {
		    throw new RuntimeException(e);
		}
	}
	public void setBackgroundImageBorderLayout(JPanel contentPane) {			//https://wall.alphacoders.com/big.php?i=178560&lang=German
	
		
		try {
		    final Image backgroundImage = ImageIO.read(getClass().getResource("wallpaper.jpg"));
		    Image newimg = backgroundImage.getScaledInstance( 900, 350,  java.awt.Image.SCALE_SMOOTH ) ; 
		    setContentPane(new JPanel(new BorderLayout()) {
		        @Override public void paintComponent(Graphics g) {
		            g.drawImage(newimg, 0, 0, null);
		        }
		    });
		   
		} catch (IOException e) {
		}
	}
	
	
}


