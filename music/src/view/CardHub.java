package view;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
 
public class CardHub { 
    static JPanel cards;
    static JPanel cardsStatic;
    private static final String PANEL2 = "PANEL2";
    private static final String PANEL1 = "PANEL1";
    private static final String PANEL3 = "PANEL3";
    private static final String PANEL4 = "PANEL4";
    private static final String PANEL5 = "PANEL5";
    private static final String PANEL6 = "PANEL6";
     
    public void addComponentToPane(Container pane) {			//Kartenerstellung zum switchen der Panels
      
        LoginPanel card1 = new LoginPanel();   
        SongAddPanel card2 = new SongAddPanel();
        SongListPanel card3 = new SongListPanel();
        Playlist card4 = new Playlist();
        ChangePW card5 = new ChangePW();
        Useradd card6 = new Useradd();
        
        cards = new JPanel(new CardLayout());
        cardsStatic = cards;
        cards.add(card1.getContentPane(), PANEL1);
        cards.add(card2.getContentPane(), PANEL2);
        cards.add(card3.getContentPane(), PANEL3);
        cards.add(card4.getContentPane(), PANEL4);
        cards.add(card5.getContentPane(), PANEL5);
        cards.add(card6.getContentPane(), PANEL6);
        
        pane.add(cards, BorderLayout.CENTER);   

    }
     
    public static void main(String[] args) {
    	
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            	JFrame frame = new JFrame("CardHub2");
            	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         
            	CardHub demo = new CardHub();
            	demo.addComponentToPane(frame.getContentPane());

            	frame.setBounds(0, 0, 900, 350);
            	frame.setVisible(true);
            	frame.setResizable(false);
            }
        });
    }
    
    public static void goToPanel2() {											//Panel switch Methode
    	  CardLayout cl = (CardLayout)(cardsStatic.getLayout());
          cl.show(cardsStatic, PANEL2);
    }
    
    public static void goToPanel1() {
  	  CardLayout cl = (CardLayout)(cardsStatic.getLayout());
      cl.show(cardsStatic, PANEL1);
  }
    
    public static void goToPanel3() {
    	  CardLayout cl = (CardLayout)(cardsStatic.getLayout());
          
          Component[] components = cardsStatic.getComponents();

          for(int i = 0; i < components.length; i++) {							//Panel neuaufrufung if contentupdate
              if(components[i].getName()!=null&&components[i].equals(PANEL3)) {
                cl.removeLayoutComponent(components[i]);
              }
          }
          SongListPanel card3 = new SongListPanel();
          cards.add(card3.getContentPane(), PANEL3); 
          cl.show(cardsStatic, PANEL3);
  
    } 
    
    public static void goToPanel4() {
    	CardLayout cl = (CardLayout)(cardsStatic.getLayout());
        
        Component[] components = cardsStatic.getComponents();

        for(int i = 0; i < components.length; i++) {							
            if(components[i].getName()!=null&&components[i].equals(PANEL4)) {
              cl.removeLayoutComponent(components[i]);
            }
        }
        Playlist card4 = new Playlist();
        cards.add(card4.getContentPane(), PANEL4); 
        cl.show(cardsStatic, PANEL4);
  } 
    
    public static void goToPanel5() {
    	  CardLayout cl = (CardLayout)(cardsStatic.getLayout());
        cl.show(cardsStatic, PANEL5);
    }
    
    public static void goToPanel6() {
  	  CardLayout cl = (CardLayout)(cardsStatic.getLayout());
      cl.show(cardsStatic, PANEL6);
  }
}