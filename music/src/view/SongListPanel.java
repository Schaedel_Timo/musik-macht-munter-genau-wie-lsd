package view;
 
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.AncestorEvent;

import logic.Logic;
import logic.Song;

public class SongListPanel extends SongAddPanel{ 

	private static JPanel contentPane;
	JButton btn_songAdd = new JButton();
	static JPanel grid;
	static JScrollPane scroll;
	static GridBagConstraints c;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SongListPanel frame = new SongListPanel();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SongListPanel() {										//MainPanel
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		setBackgroundImageBorderLayout(contentPane);
		contentPane = (JPanel)getContentPane();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		JPanel navPanel = new JPanel();								//NavigationsPanel
		navPanel.setLayout(new FlowLayout());
		navPanel.add(btn_logout);
		navPanel.add(btn_songAdd);
		navPanel.add(btn_changePW);
		navPanel.add(btn_checkadmin);
		navPanel.add(btn_addUser);
		navPanel.setOpaque(false);
		contentPane.add(navPanel, BorderLayout.NORTH);
		
		grid = new JPanel();										//SonglistenPanel
		c = new GridBagConstraints();
		grid.setLayout(new GridBagLayout());
		grid.setBackground(Color.decode("#EFCE74"));
		
		c.weightx = 0.5;
		c.ipadx = 20;
		c.ipady = 10;
		c.gridx = 0;
		c.gridy = 0;
		grid.add(new JLabel("VoteKnopf"), c);
		
		c.gridx = 1;
		c.gridy = 0;
		grid.add(new JLabel("VoteCount"), c);
		
		c.gridx = 2;
		c.gridy = 0;
		grid.add(new JLabel("Bandname"), c);
		
		c.gridx = 3;
		c.gridy = 0;
		grid.add(new JLabel("Titel"), c);
		
		c.gridx = 4;
		c.gridy = 0;
		grid.add(new JLabel("Genre"), c);
		
		int y = 1;												//Listeninitialisierung
		ArrayList<Song> songs = (ArrayList<Song>) Logic.getSongs();
		for (Song s: songs) {
			
			JButton button = new JButton("+");
			JLabel count = new JLabel("" + data.VoteTools.getVotesfor(s.getId()));
			
			c.gridx = 0;
			c.gridy = y;
			grid.add(button, c);

			c.gridx = 1;
			c.gridy = y;
			grid.add(count, c);

			c.gridx = 2;
			c.gridy = y;
			grid.add(new JLabel(s.getBandName()), c);

			c.gridx = 3;
			c.gridy = y;
			grid.add(new JLabel(s.getTitelName()), c);

			c.gridx = 4;
			c.gridy = y;
			grid.add(new JLabel(s.getGenre()), c);

			y++;
			
			button.addActionListener(new ActionListener() {			//Neuladen falls runtime change
				public void actionPerformed (ActionEvent e){
					logic.Logic.addVote(s.getId());
					CardHub.goToPanel3();
				}
			});
		}

		scroll = new JScrollPane(grid);								//scrollbar
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		contentPane.add(scroll, BorderLayout.CENTER);

		
		try {							//https://www.audiosparx.com/sa/module/images/controls/icon_tracks.png
		    Image img = ImageIO.read(getClass().getResource("musiknote.png"));
		    Image newimg = img.getScaledInstance( 20, 20,  java.awt.Image.SCALE_SMOOTH ) ; 
		    btn_songAdd.setIcon(new ImageIcon(newimg));
		    
		  } catch (Exception ex) {
		    System.out.println(ex);
		  }
		
		btn_songAdd.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent e){
				CardHub.goToPanel2();
			}
		});
	}
}
