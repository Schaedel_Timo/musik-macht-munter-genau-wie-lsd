package logic;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import data.SongTools;
import data.UserTools;
import data.VoteTools;
import view.LoginPanel;

public class Logic {

	//Adds a song to database
	public static void addSong(String titel,String band,String genre) {
		
	SongTools.create(new Song(titel,band,genre,titel.hashCode()));
				
	}
	
	
	//Adds a vote to the database
	public static void addVote(int id) {
		long time=System.currentTimeMillis();
		String benutzer= LoginPanel.getTxtlogin();
		System.out.println("------" + VoteTools.getUserVotes(benutzer) + "" + VoteTools.getFirstVote(benutzer));
		if(VoteTools.getUserVotes(benutzer)>=5) {
			
			//If user has more than 5 votes,
			
			VoteTools.delete( VoteTools.getFirstVote(benutzer) );//Delete the first vote
		}
		
		VoteTools.create(time,id, benutzer);
		
	}
	
	
	
	
	
	//Returns list of songs
	public static List<Song> getSongs(){
		
		List<Song> songListe=new ArrayList<Song>();
		
		for(int i=0;i<SongTools.getSongCount();i++) {
			
			Song s = SongTools.read(i);
			songListe.add(s);
			
		}
		return songListe;
	}
	

	//Returns list with songs and votes bound to each other
	public static ArrayList<SongAndVoteWrapper> getSortedListWithVotes(){
	
		ArrayList<Song> songList=new ArrayList<Song>();
		for(int i=0; i<SongTools.getSongCount();i++) {
			Song s= SongTools.read(i);//If it doesnt have 0 votes, add it
			if(VoteTools.getVotesfor(s.getId())!=0) songList.add(s);
			
		}
		
		Collections.sort(songList,new Comparator<Song>(){
			public int compare(Song s1, Song s2) {
				return VoteTools.getVotesfor(s2.getId()) -  VoteTools.getVotesfor(s1.getId());
			}
		});
		
		ArrayList<SongAndVoteWrapper> songVoteList = new ArrayList<>();
		
		for(Song s:songList ) {
			songVoteList.add(new SongAndVoteWrapper(s,VoteTools.getVotesfor(s.getId())));
			//System.out.println("SongAndVoteList:"+s+"   Votes:"+VoteTools.getVotesfor(s.getId()));
		}
		
		
		return songVoteList;
	}
	
	
	//Checks if password is correct/Lets through
	public static boolean checkPassword(String benutzer, String passwort) {
		if(UserTools.checkPwEmpty(benutzer))return true;//If empty, let through

		return UserTools.checkPassword(benutzer,getHashedPasswordWithUserName(benutzer,passwort));
	}
	
	public static boolean updatePassword(String benutzer,String altesPW, String neuesPasswort) {
		//checkPW
		if(checkPassword(benutzer,altesPW)) {
			UserTools.updatePW(benutzer, getHashedPasswordWithUserName(benutzer,neuesPasswort));
			return true;
		}
		return false;

	}
	//returns hashed pw for security
	public static String getHashedPasswordWithUserName(String user,String password) {
		return password.hashCode()+user.length()+user.charAt(user.length()/2)+420+"";
	}
	
	
	
	
	
	

}
