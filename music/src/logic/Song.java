package logic;

public class Song {
	//Describes a SOng

private String 
band,
titel,
genre;

private int id;


public Song(String titelName, String bandName, String genre, int id ) {
this.band = bandName;
this.titel = titelName;
this.genre = genre;
this.id = id;
}
public Song() {}

public String getBandName() {return band;}
public void setBandName(String bandName) {this.band = bandName;}
public String getTitelName() {return titel;}
public void setTitelName(String titelName) {this.titel = titelName;}
public String getGenre() {return genre;}
public void setGenre(String genre) {this.genre = genre;}
public int getId() {return id;}
public void setId(int id) {this.id = id;}

@Override
public String toString() {
	return "Song [band=" + band + ", titel=" + titel + ", genre=" + genre + ", id=" + id + "]";
}


}