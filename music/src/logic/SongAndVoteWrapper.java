package logic;

public class SongAndVoteWrapper {
	
	//Hilfsobjekt, packt Votes mit Songs zusammen, damit man sie in eine einfache Liste packen kann
	
	private Song song;
	private int votes;
	
	
	public SongAndVoteWrapper(Song s, int votes) {
		this.song=s;
		this.votes=votes;
	}
	
	public Song getSong() {
		return song;
	}
	public void setSong(Song s) {
		this.song = s;
	}
	public int getVotes() {
		return votes;
	}
	public void setVotes(int votes) {
		this.votes = votes;
	}

}
