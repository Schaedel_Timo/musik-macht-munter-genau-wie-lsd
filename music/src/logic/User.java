package logic;

public class User {
	//Describes a User

	private static String user="";
	private static String titel="";
	
	//Checks if user exists
	public static boolean checkIfUserExists(String user) {
		return data.UserTools.checkUser(user);
	}
	
	public static String getUser() {
		
		if(!user.isEmpty()) {
			return user;
		}else {
			throw new RuntimeException("User not set");
		}
		
		
	}
	
	public static String getTitel() {
		if(!titel.isEmpty()) {
			return titel;
		}else {
			throw new RuntimeException("User not set");
		}
	}
	
	public static boolean setUserAndCheck(String user) {
		// Überprüfen ob benutzer akzeptiert wird
		if(checkIfUserExists(user)) {//Dann setzen
			User.user = user;
			return true;
		}
		return false;
	}
	
	public static void setTitel(String titel) {
		if(!user.isEmpty()) {//Weil davor schon gesetzt wurde
			User.titel = titel;
			return;
		}
		throw new RuntimeException("Set User first");
	}
	
}
