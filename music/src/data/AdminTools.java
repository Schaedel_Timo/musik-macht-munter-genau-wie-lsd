package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import logic.Song;

public class AdminTools {

	
	private static String driver = "com.mysql.jdbc.Driver", url = "jdbc:mysql://localhost/musikvoting?", user = "root",
			passwd = "";// "superuser";

	protected static Connection con;
	static Statement stmt;
	static ResultSet rs;
	static PreparedStatement PS;
	protected int _index_ = 0;

	Song _song = new Song();
	private static boolean setup=false;
	
	private static boolean setup() {
		if(setup)return true;
		try {
			Class.forName(driver);
			// connect

			con = DriverManager.getConnection(url, user, passwd);
			stmt = con.createStatement();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("error loading driver and to connect in votes!");
			System.exit(0);
		}
		setup=true;
		return true;
	}

	public static void addUserToDB(String name, String titel) {
		setup();
		
		try {
			PS = con.prepareStatement("INSERT INTO T_USER (p_name, adelstitel) VALUES (?,?);");
			PS.setString(1, name);
			PS.setString(2, titel);
			PS.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("\n" + "failed to use create method");
		}
	}
	
	
	
	
	
}

