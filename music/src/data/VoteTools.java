package data;

import java.lang.*; // time millis
import java.util.Date;

import logic.Song;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class VoteTools {

	private static String driver = "com.mysql.jdbc.Driver", url = "jdbc:mysql://localhost/musikvoting?", user = "root",
			passwd = "";// "superuser";

	protected static Connection con;
	static Statement stmt;
	static ResultSet rs;
	static PreparedStatement PS;
	protected int _index_ = 0;

	Song _song = new Song();
	private static boolean setup=false;
	
	private static boolean setup() {
		if(setup)return true;
		try {
			Class.forName(driver);
			// connect

			con = DriverManager.getConnection(url, user, passwd);
			stmt = con.createStatement();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("error loading driver and to connect in votes!");
			System.exit(0);
		}
		setup=true;
		return true;
	}
		
	

/*	public int create(String titel, long time) {

		try {
			// CMD
			PS = con.prepareStatement("INSERT INTO T_VOTE (p_titel,p_uhrzeit) VALUES (?,?);");

			PS.setString(1, titel);
			PS.setLong(2, time);

			PS.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("\n" + "failed to execute querie!");
			return 0;
		}

		return 1;

	}*/
	//Checks if user has adelstitel
	public static boolean userhasadelstitel(String benutzer) {
		
		try {
			rs = stmt.executeQuery("SELECT adelstitel from T_User Where p_name=" + "'" + benutzer + "'" + "AND adelstitel is not null;");
			return rs.next();
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	//Adds a Vote to DB
	public static void create(long time, int songId, String benutzer) {
		setup();
		if (userhasadelstitel(benutzer)) {
		try {
			PS = con.prepareStatement("INSERT INTO T_VOTE (p_uhrzeit, p_id_song, p_name) VALUES (?,?,?);");
			PS.setLong(1, time);
			PS.setInt(2, songId);
			PS.setString(3, benutzer);
			PS.executeUpdate();
			
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("\n" + "failed to use create method");
			
		}
		}
	}
	
	/*public void delete(int index) {

		ResultSet rs;
		//int numColumns=rs.getMetaData().getColumnCount();;

		try {

			int votes_ = getVoteCount();

			String id = "";
			long time = 0;

			rs = stmt.executeQuery("SELECT p_titel,p_uhrzeit FROM T_VOTE;");

			for (int x = 0; x < votes_; x++) {

				while (rs.next()) {
					for (int i = 0; i < 2; i++) {
						if (i == 0)
							id = rs.getObject(i + 1).toString();
						else if (i == 1)
							time = Long.parseLong(((rs.getObject(i + 1).toString())));
					}
					stmt.executeUpdate(
							"DELETE FROM T_VOTE WHERE p_titel=" + "'" + id + "'" + " AND p_uhrzeit=" + time + ";");
				}
			}

		} catch (Exception e) {
		}
	}*/
	
	//Deletes a Vote from DB
	public static void delete(long time) {
		setup();
		try {
			stmt.executeUpdate("DELETE FROM T_VOTE WHERE p_uhrzeit=" + "'" + time + "'" + ";");
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("\n" + "delete()");
		}
		
	}

	
	//Gets time (PRIMARY KEY) of first vote
	public static long getFirstVote(String benutzer) {
		setup();
		long time = 0;
		try {
			rs = stmt.executeQuery("SELECT p_uhrzeit, p_id_song FROM T_VOTE WHERE p_name=" + "'" + benutzer + "'" + "Order by p_uhrzeit" + ";");
			rs.next();
			time = rs.getLong(1);
			System.out.println(time);
		} catch (SQLException e) {
		}
		return time;
	}
	
	//Gets Vote amount for a specific SONG
	public static int getVotesfor(int songId) {
		setup();
		int votes = 0;
		try {
			rs = stmt.executeQuery("Select Count(p_id_song) FROM T_VOTE WHERE p_id_song =" + songId + ";");
			rs.next();
			votes = rs.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("\n" + "getVotesfor()");
		}
		return votes;
	}
	
	//Gets amount of votes from USer
	public static int getUserVotes(String benutzer) {
		setup();
		int count = 0;
		try {
			rs = stmt.executeQuery("Select Count(p_id_song) FROM T_VOTE WHERE p_name =" + "'" + benutzer  + "'" + ";");
			rs.next();
			count = rs.getInt(1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return count;
	}

	//Gets amount of votes
	public static int getVoteCount() {
		setup();

		int vote_counter = 0;

		try {
			ResultSet rs;
			rs = stmt.executeQuery("SELECT COUNT(*) AS count FROM T_VOTE;");

			while (rs.next())
				vote_counter = rs.getInt("count");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return vote_counter;
	}

}
