package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import view.LoginPanel;

public class UserTools {

	private static String driver = "com.mysql.jdbc.Driver", url = "jdbc:mysql://localhost/musikvoting?", user = "root",
			passwd = "";

	static Connection con;
	static Statement stmt;
	static ResultSet rs;
	static PreparedStatement PS;
	static boolean setup=false;

	
	//Sets up Connection
	public static void setup() {
		setup=true;
		System.out.println("Aufgerufen");
		try {
			Class.forName(driver);
			// connect

			con = DriverManager.getConnection(url, user, passwd);
			stmt = con.createStatement();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("error loading driver and to connect in votes!");
			System.exit(0); 
		}
	}

	
	//Updates a Password via SQL
	
	public static void updatePW(String benutzer, String neuesPW) {
		if(!setup)setup();
		try {
			stmt.executeUpdate("Update T_User Set passwort=" + "'" + neuesPW + "'" + "WHERE p_name=" + "'" + benutzer + "'" + ";");
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	//Checks if a user exists in SQL
	public static boolean checkUser(String u_name) {

		if(!setup)setup();
		try {
			rs = stmt.executeQuery("SELECT p_name FROM T_USER WHERE p_name="+"'"+u_name+"';");
			
			return rs.next();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	//Checks if a specific password matches a user
	public static boolean checkPassword(String benutzer,String password) {
		
		try {
			rs = stmt.executeQuery("Select Passwort FROM T_User WHERE p_name=" +  "'" + benutzer + "'" + "AND Passwort=" + "'" + password + "'");
			return rs.next();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	//Checks if a password exists so user can go through login screen when none is set
	
	public static boolean checkPwEmpty(String benutzer) {
		
		try {
			rs = stmt.executeQuery("Select Passwort FROM T_User WHERE p_name=" +  "'" + benutzer + "' AND Passwort is NULL");
			return rs.next();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}


}
