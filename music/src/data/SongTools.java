package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Arrays;

import logic.Song;

public class SongTools {

	private static String driver = "com.mysql.jdbc.Driver", 
			url = "jdbc:mysql://localhost/musikvoting?",
			user = "root",
			passwd = "";// "superuser";

	protected static Connection con;
	static Statement stmt;
	static ResultSet rs;
	static PreparedStatement PS;

	private static boolean setup=false; 
	
	
	//Sets up
	public static boolean setup() {
		if(setup)return true;
		
		try {
			Class.forName(driver);
			// connect

			con = DriverManager.getConnection(url, user, passwd);
			stmt = con.createStatement();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Please check your connection and Database");
			System.exit(0);
		}
		 setup= true;
		return true;
		
	}

	//Adds a Song to the Database
	public static int create(Song s) {
		setup();

		try {
			// CMD
			PS = con.prepareStatement("INSERT INTO T_SONG (p_id_song,p_bandname,p_titel,genre) VALUES (?,?,?,?);");

			PS.setInt(1,s.getId() );
			PS.setString(2, s.getBandName());
			PS.setString(3, s.getTitelName());
			PS.setString(4, s.getGenre());

			PS.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("\n" + "failed to execute query!");
		}
		
		System.out.println("SONG CREATED ::: "+s);
		
		int all_indexes_song = getSongCount();
		return ++all_indexes_song; // increment index counter

	}

	
	//Reads a SOng from the Database
	public static Song read(int index) {
		setup();
		ResultSet rs;
		String str = "";

		String values[] = { "", "", "" ,""};
		
		int numColumns;
		Song temp_song=null;
		try {

			int songs_ = getSongCount();
			Song[] song_arr = new Song[songs_];
			
			rs = stmt.executeQuery("SELECT p_titel,p_bandname,genre,p_id_song FROM T_SONG;");
			numColumns = rs.getMetaData().getColumnCount();
			
			for (int x = 0; x <songs_; x++) {

				while (rs.next()) {
					for (int i = 0; i <values.length; i++) {
						
						for ( int y = 1 ; y <= numColumns ; y++ ) {
							
							try {
								str = rs.getObject(y).toString();
							}catch(NullPointerException np) {
								str="";
							}
							values[y-1] = str;
						}
					}
					
					temp_song = new Song(values[0], values[1], values[2], Integer.parseInt(values[3]));
					song_arr[x++] = temp_song;
				}
			}
			return song_arr[index];

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("\nfailed to read song!");
		}
		return null;
	}
	
	//Gets amount of songs from DATABASE
	public static int getSongCount() {
		setup();

		int song_counter = 0;

		try {
			ResultSet rs;
			rs = stmt.executeQuery("SELECT COUNT(*) AS counter FROM T_SONG;");
			
			while(rs.next())
				song_counter = rs.getInt("counter");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return song_counter;
	}
	

}
